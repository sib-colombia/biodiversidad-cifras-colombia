//funciones para ver referencias e info

function showDiv2(currentElement) {
    $('.referencia').css('display', 'none');
    $(currentElement)
      .parent()
      .parent()
      .next('.referencia')
      .css('display', 'block');
}

function showDiv(currentElement) {
    $('.referencia').css('display', 'none');
    $(currentElement)
      .parent()
      .next('.referencia')
      .css('display', 'block');
}

function showDiv0(currentElement) {
    $('.referencia').css('display', 'none');
    $(currentElement)
      .next('.referencia')
      .css('display', 'block');
}

function showDiv2nd(currentElement) {
    $('.referencia').css('display', 'none');
    $(currentElement)
      .parent()
      .nextAll('.2nd')
      .css('display', 'block');
}

function showDiv3rd(currentElement) {
    $('.referencia').css('display', 'none');
    $(currentElement)
      .parent()
      .nextAll('.3rd')
      .css('display', 'block');
}

function showDiv4th(currentElement) {
    $('.referencia').css('display', 'none');
    $(currentElement)
      .parent()
      .nextAll('.4th')
      .css('display', 'block');
}

function showDiv5th(currentElement) {
    $('.referencia').css('display', 'none');
    $(currentElement)
      .parent()
      .nextAll('.5th')
      .css('display', 'block');
}

function showDiv6th(currentElement) {
    $('.referencia').css('display', 'none');
    $(currentElement)
      .parent()
      .nextAll('.6th')
      .css('display', 'block');
}

function hideDiv(currentElement) {
    $(currentElement)
      .parent()
      .css('display', 'none');
}

function hideDiv2(currentElement) {
    $(currentElement)
      .parent()
      .parent()
      .parent()
      .css('display', 'none');
}

//funciones para graficas de especies y de tercer nivel

var options_graph1 =  {
    titlePosition: 'none',
    pieHole: 0.7,
    backgroundColor: 'transparent',
    legend: {position: 'none'},
    chartArea: {width: '220', height: '220'},
    pieSliceBorderColor: 'none',
    slices: {0: {color: '#4BC0C0'}},
    sliceVisibilityThreshold: '0',
    fontSize: '12',
    pieSliceText: 'none',
    tooltip: {trigger: 'none'}
};

var options_graph1b = {
    titlePosition: 'none',
    pieHole: 0.9,
    backgroundColor: 'transparent',
    legend: {position: 'none'},
    chartArea: {width: '222', height: '222'},
    pieSliceBorderColor: 'none',
    slices: {0: {color: '#2B4143'}, 1: {color: '#e7e9ed'}},
    sliceVisibilityThreshold: '0',
    fontSize: '12',
    pieSliceText: 'none',
    tooltip: {isHtml: true, textStyle: {color: '#fff'}}
};

var options_graph2 = {
    titlePosition: 'none',
    pieHole: 0.7,
    backgroundColor: 'transparent',
    legend: {position: 'none'},
    chartArea: {width: '180', height: '180'},
    pieSliceBorderColor: 'none',
    slices: {
        0: {color: '#d51920'},
        1: {color: '#f47d20'},
        2: {color: '#f9ba1b'}
    },
    sliceVisibilityThreshold: '0',
    fontSize: '12',
    pieSliceText: 'none',
    tooltip: {trigger: 'none'}
};

var options_graph2b = {
    titlePosition: 'none',
    pieHole: 0.9,
    backgroundColor: 'transparent',
    legend: {position: 'none'},
    chartArea: {width: '222', height: '222'},
    pieSliceBorderColor: 'none',
    slices: {
        0: {color: '#d51920'},
        1: {color: '#f47d20'},
        2: {color: '#f9ba1b'},
        3: {color: '#e7e9ed'}
    },
    sliceVisibilityThreshold: '0',
    fontSize: '12',
    pieSliceText: 'none',
    tooltip: {isHtml: true, textStyle: {color: '#fff'}}
};

var options_graph3 = {
    titlePosition: 'none',
    pieHole: 0.7,
    backgroundColor: 'transparent',
    legend: {position: 'none'},
    chartArea: {width: '180', height: '180'},
    pieSliceBorderColor: 'none',
    slices: {
        0: {color: '#238ED9'},
        1: {color: '#A2C754'},
        2: {color: '#E8702F'}
    },
    sliceVisibilityThreshold: '0',
    fontSize: '12',
    pieSliceText: 'none',
    tooltip: {trigger: 'none'}
};

var options_graph3b = {
    titlePosition: 'none',
    pieHole: 0.9,
    backgroundColor: 'transparent',
    legend: {position: 'none'},
    chartArea: {width: '222', height: '222'},
    pieSliceBorderColor: 'none',
    slices: {
        0: {color: '#238ED9'},
        1: {color: '#A2C754'},
        2: {color: '#E8702F'},
        3: {color: '#e7e9ed'}
    },
    sliceVisibilityThreshold: '0',
    fontSize: '12',
    pieSliceText: 'none',
    tooltip: {isHtml: true, textStyle: {color: '#fff'}}
};

var options_graph3c = {
    titlePosition: 'none',
    pieHole: 0.7,
    backgroundColor: 'transparent',
    legend: {position: 'none'},
    chartArea: {width: '180', height: '180'},
    pieSliceBorderColor: 'none',
    slices: {
        0: {color: '#238ED9'},
        1: {color: '#46d9c2'},
        2: {color: '#A2C754'},
        3: {color: '#E8702F'}
    },
    sliceVisibilityThreshold: '0',
    fontSize: '12',
    pieSliceText: 'none',
    tooltip: {trigger: 'none'}
};
var options_graph3bc = {
    titlePosition: 'none',
    pieHole: 0.9,
    backgroundColor: 'transparent',
    legend: {position: 'none'},
    chartArea: {width: '222', height: '222'},
    pieSliceBorderColor: 'none',
    slices: {
        0: {color: '#238ED9'},
        1: {color: '#46d9c2'},
        2: {color: '#A2C754'},
        3: {color: '#E8702F'},
        4: {color: '#e7e9ed'}
    },
    sliceVisibilityThreshold: '0',
    fontSize: '12',
    pieSliceText: 'none',
    tooltip: {isHtml: true, textStyle: {color: '#fff'}}
};




function showDivGraph(currentElement) {
    $('.grapharea').css('display', 'none');
    $(currentElement)
      .parent()
      .next('.grapharea')
      .css('display', 'block');

    // pie de literatura especies mamiferos

    google.charts.load('current', {packages: ['corechart']});

    // pie de literatura especies animales

    google.charts.setOnLoadCallback(Chartanimales1);

    function Chartanimales1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutanimales1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartanimales1b);

    //Grafica general animales
    function Chartanimales1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 31780],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutanimales1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartanimales2);

    //Grafica estimadas amenaza animales
    function Chartanimales2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 62],
            ['En peligro', 159],
            ['Vulnerable', 262]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutanimales2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartanimales2b);

    //Grafica SiB amenaza animales
    function Chartanimales2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                52,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 52</br>Registros: 23.598</div>'
            ],
            [
                'En peligro SiB',
                138,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 138</br>Registros: 49.743</div>'
            ],
            [
                'Vulnerable SiB',
                241,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 241</br>Registros: 120.188</div>'
            ],
            ['Especies sin datos SiB Colombia', 52, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutanimales2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartanimales3);

    //Grafica estimadas CITES animales
    function Chartanimales3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 52],
            ['Apéndice I/II', 9],
            ['Apéndice II', 604],
            ['Apéndice III', 30]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutanimales3')
        );
        chart.draw(data, options_graph3c);
    }

    google.charts.setOnLoadCallback(Chartanimales3b);

    //Grafica SiB CITES animales
    function Chartanimales3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                45,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 45</br>Registros: 30.376</div>'
            ],
            [
                'Apéndice I/II en SiB',
                1,
                '<div class="pad10"><b>Apéndice I/II en SiB Colombia</b></br>Especies: 1</br>Registros: 76</div>'
            ],
            [
                'Apéndice II en SiB',
                536,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 536</br>Registros: 1.364.010</div>'
            ],
            [
                'Apéndice III en SiB',
                30,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 30</br>Registros: 60.438</div>'
            ],
            ['Especies sin datos SiB Colombia', 74, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutanimales3b')
        );
        chart.draw(data, options_graph3bc);
    }

    // pie de literatura especies mamiferos

    google.charts.setOnLoadCallback(ChartMam1);

    function ChartMam1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 543]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmam1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(ChartMam1b);

    //Grafica general mamíferos 
    function ChartMam1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 737]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmam1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(ChartMam2);

    //Grafica estimada amenaza mamíferos 
    function ChartMam2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 7],
            ['En peligro', 10],
            ['Vulnerable', 25]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmam2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(ChartMam2b);

    //Grafica SiB amenaza mamíferos 
    function ChartMam2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                3,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 3</br>Registros: 877</div>'
            ],
            [
                'En peligro SiB',
                9,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 9</br>Registros: 1.036</div>'
            ],
            [
                'Vulnerable SiB',
                21,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 21</br>Registros: 10.202 </div>'
            ],
            ['Especies sin datos SiB Colombia', 9, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmam2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(ChartMam3);

    //Grafica estimadas CITES mamíferos 
    function ChartMam3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 29],
            ['Apéndice II', 65],
            ['Apéndice III', 7]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmam3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(ChartMam3b);

    //Grafica SiB CITES mamíferos 
    function ChartMam3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                27,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 27</br>Registros: 13.583</div>'
            ],
            [
                'Apéndice II en SiB',
                55,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 55</br>Registros: 17.931</div>'
            ],
            [
                'Apéndice III en SiB',
                7,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 7</br>Registros: 13.402</div>'
            ],
            ['Especies sin datos SiB Colombia', 12, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmam3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies plantas

    google.charts.setOnLoadCallback(Chartplantas1);

    function Chartplantas1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutplantas1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartplantas1b);

    //Grafica general plantas 
    function Chartplantas1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 36910],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutplantas1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartplantas2);

    //Grafica estimada amenaza plantas 
    function Chartplantas2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 120],
            ['En peligro', 269],
            ['Vulnerable', 425]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutplantas2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartplantas2b);

    //Grafica SiB amenaza plantas 
    function Chartplantas2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                114,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 114</br>Registros: 3.219 </div>'
            ],
            [
                'En peligro SiB',
                249,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 249</br>Registros: 25.568</div>'
            ],
            [
                'Vulnerable SiB',
                383,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 383</br>Registros: 22.408</div>'
            ],
            ['Especies sin datos SiB Colombia', 68, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutplantas2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartplantas3);

    //Grafica estimadas CITES plantas 
    function Chartplantas3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 11],
            ['Apéndice II', 2847],
            ['Apéndice III', 1]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutplantas3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartplantas3b);

    //Grafica SiB CITES plantas 
    function Chartplantas3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                8,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 8</br>Registros: 187</div>'
            ],
            [
                'Apéndice II en SiB',
                2121,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 2.121</br>Registros: 62.060</div>'
            ],
            [
                'Apéndice III en SiB',
                1,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 1</br>Registros: 103</div>'
            ],
            ['Especies sin datos SiB Colombia', 729, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutplantas3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies aves

    google.charts.setOnLoadCallback(Chartave1);

    function Chartave1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 1954]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutave1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartave1b);

    //Grafica general aves 
    function Chartave1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 2344],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutave1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartave2);

    //Grafica estimada amenaza aves 
    function Chartave2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 17],
            ['En peligro', 56],
            ['Vulnerable', 67]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutave2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartave2b);

    //Grafica SiB amenaza aves 
    function Chartave2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                15,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 15</br>Registros: 9.744</div>'
            ],
            [
                'En peligro SiB',
                52,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 52</br>Registros: 43.516</div>'
            ],
            [
                'Vulnerable SiB',
                64,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 64</br>Registros: 92.879</div>'
            ],
            ['Especies sin datos SiB Colombia', 9, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutave2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartave3);

    //Grafica estimadas CITES aves 
    function Chartave3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 10],
            ['Apéndice II', 306],
            ['Apéndice III', 14]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutave3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartave3b);

    //Grafica SiB CITES aves 
    function Chartave3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                10,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 10</br>Registros: 13.342</div>'
            ],
            [
                'Apéndice II en SiB',
                275,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 275</br>Registros: 1.231.889</div>'
            ],
            [
                'Apéndice III en SiB',
                14,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 14</br>Registros: 45.985</div>'
            ],
            ['Especies sin datos SiB Colombia', 31, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutave3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies anfibios

    google.charts.setOnLoadCallback(Chartanfibio1);

    function Chartanfibio1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutanfibio1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartanfibio1b);

    //Grafica general anfibios 
    function Chartanfibio1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 887],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutanfibio1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartanfibio2);

    //Grafica estimada amenaza anfibios 
    function Chartanfibio2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 14],
            ['En peligro', 23],
            ['Vulnerable', 13]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutanfibio2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartanfibio2b);

    //Grafica SiB amenaza anfibios 
    function Chartanfibio2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                14,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 14</br>Registros: 2.207</div>'
            ],
            [
                'En peligro SiB',
                22,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 22</br>Registros: 605</div>'
            ],
            [
                'Vulnerable SiB',
                13,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 13</br>Registros: 1.511</div>'
            ],
            ['Especies sin datos SiB Colombia', 1, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutanfibio2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartanfibio3);

    //Grafica estimadas CITES anfibios 
    function Chartanfibio3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 39],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutanfibio3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartanfibio3b);

    //Grafica SiB CITES anfibios 
    function Chartanfibio3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                36,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 36</br>Registros: 12.332</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 3, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutanfibio3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies reptiles

    google.charts.setOnLoadCallback(Chartreptil1);

    function Chartreptil1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutreptil1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartreptil1b);

    //Grafica general reptiles 
    function Chartreptil1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 752],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutreptil1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartreptil2);

    //Grafica estimada amenaza reptiles 
    function Chartreptil2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 11],
            ['En peligro', 16],
            ['Vulnerable', 17]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutreptil2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartreptil2b);

    //Grafica SiB amenaza reptiles 
    function Chartreptil2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                10,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 10</br>Registros: 8.283</div>'
            ],
            [
                'En peligro SiB',
                16,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 16</br>Registros: 3.322 </div>'
            ],
            [
                'Vulnerable SiB',
                14,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 14</br>Registros: 1.974</div>'
            ],
            ['Especies sin datos SiB Colombia', 4, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutreptil2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartreptil3);

    //Grafica estimadas CITES reptiles 
    function Chartreptil3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 9],
            ['Apéndice I/II', 4],
            ['Apéndice II', 29],
            ['Apéndice III', 2]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutreptil3')
        );
        chart.draw(data, options_graph3c);
    }

    google.charts.setOnLoadCallback(Chartreptil3b);

    //Grafica SiB CITES reptiles 
    function Chartreptil3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                7,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 7</br>Registros: 3.450</div>'
            ],
            [
                'Apéndice I/II en SiB',
                1,
                '<div class="pad10"><b>Apéndice I/II en SiB Colombia</b></br>Especies: 1</br>Registros: 76</div>'
            ],
            [
                'Apéndice II en SiB',
                27,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 27</br>Registros: 14.143</div>'
            ],
            [
                'Apéndice III en SiB',
                2,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 2</br>Registros: 292</div>'
            ],
            ['Especies sin datos SiB Colombia', 2, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutreptil3b')
        );
        chart.draw(data, options_graph3bc);
    }

    // pie de literatura especies peces

    google.charts.setOnLoadCallback(Chartpeces1);

    function Chartpeces1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpeces1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartpeces1b);

    //Grafica general peces 
    function Chartpeces1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 4155],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpeces1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartpeces2);

    //Grafica estimada amenaza peces 
    function Chartpeces2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 7],
            ['En peligro', 10],
            ['Vulnerable', 92]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpeces2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartpeces2b);

    //Grafica SiB amenaza peces 
    function Chartpeces2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                5,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 5</br>Registros: 2.138</div>'
            ],
            [
                'En peligro SiB',
                9,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 9</br>Registros: 570</div>'
            ],
            [
                'Vulnerable SiB',
                87,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 87</br>Registros: 7.493</div>'
            ],
            ['Especies sin datos SiB Colombia', 8, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpeces2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartpeces3);

    //Grafica estimadas CITES peces 
    function Chartpeces3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 3],
            ['Apéndice II', 18],
            ['Apéndice III', 6]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpeces3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartpeces3b);

    //Grafica SiB CITES peces 
    function Chartpeces3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                1,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 1</br>Registros: 1</div>'
            ],
            [
                'Apéndice II en SiB',
                14,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 14</br>Registros: 179</div>'
            ],
            [
                'Apéndice III en SiB',
                6,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 6</br>Registros: 757</div>'
            ],
            ['Especies sin datos SiB Colombia', 6, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpeces3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies insectos

    google.charts.setOnLoadCallback(Chartinsecto1);

    function Chartinsecto1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutinsecto1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartinsecto1b);

    //Grafica general insectos 
    function Chartinsecto1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 14573],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutinsecto1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartinsecto2);

    //Grafica estimada amenaza insectos 
    function Chartinsecto2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 5],
            ['En peligro', 15],
            ['Vulnerable', 16]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutinsecto2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartinsecto2b);

    //Grafica SiB amenaza insectos 
    function Chartinsecto2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                4,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 4</br>Registros: 95</div>'
            ],
            [
                'En peligro SiB',
                10,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 10</br>Registros: 126</div>'
            ],
            [
                'Vulnerable SiB',
                12,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 12</br>Registros: 377</div>'
            ],
            ['Especies sin datos SiB Colombia', 10, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutinsecto2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartinsecto3);

    //Grafica estimadas CITES insectos 
    function Chartinsecto3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutinsecto3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartinsecto3b);

    //Grafica SiB CITES insectos 
    function Chartinsecto3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutinsecto3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies aracnidos

    google.charts.setOnLoadCallback(Chartaracnidos1);

    function Chartaracnidos1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutaracnidos1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartaracnidos1b);

    //Grafica general arácnidos 
    function Chartaracnidos1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 1.308],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutaracnidos1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartaracnidos2);

    //Grafica estimada amenaza arácnidos 
    function Chartaracnidos2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 3],
            ['Vulnerable', 5]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutaracnidos2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartaracnidos2b);

    //Grafica SiB amenaza arácnidos 
    function Chartaracnidos2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                3,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 3</br>Registros: 52</div>'
            ],
            [
                'Vulnerable SiB',
                5,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 5</br>Registros: 251</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutaracnidos2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartaracnidos3);

    //Grafica estimadas CITES arácnidos 
    function Chartaracnidos3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutaracnidos3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartaracnidos3b);

    //Grafica SiB CITES arácnidos 
    function Chartaracnidos3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutaracnidos3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies moluscos

    google.charts.setOnLoadCallback(Chartmolusco1);

    function Chartmolusco1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmolusco1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartmolusco1b);

    //Grafica general moluscos 
    function Chartmolusco1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 3084],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmolusco1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartmolusco2);

    //Grafica estimada amenaza moluscos 
    function Chartmolusco2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 14]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmolusco2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartmolusco2b);

    //Grafica SiB amenaza moluscos 
    function Chartmolusco2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                12,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 12</br>Registros: 1932</div>'
            ],
            ['Especies sin datos SiB Colombia', 2, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmolusco2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartmolusco3);

    //Grafica estimadas CITES moluscos 
    function Chartmolusco3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 1],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmolusco3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartmolusco3b);

    //Grafica SiB CITES moluscos 
    function Chartmolusco3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0 </div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 1, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmolusco3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies decápodos

    google.charts.setOnLoadCallback(Chartdecapodo1);

    function Chartdecapodo1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodo1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartdecapodo1b);

    //Grafica general decápodos 
    function Chartdecapodo1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 1254],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodo1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartdecapodo2);

    //Grafica estimada amenaza decápodos 
    function Chartdecapodo2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 25],
            ['Vulnerable', 8]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodo2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartdecapodo2b);

    //Grafica SiB amenaza decápodos 
    function Chartdecapodo2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                16,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 16</br>Registros: 98</div>'
            ],
            [
                'Vulnerable SiB',
                8,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 8</br>Registros: 762 </div>'
            ],
            ['Especies sin datos SiB Colombia', 9, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodo2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartdecapodo3);

    //Grafica estimadas CITES decápodos 
    function Chartdecapodo3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodo3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartdecapodo3b);

    //Grafica SiB CITES decápodos 
    function Chartdecapodo3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);



        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodo3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies crustaceosm

    google.charts.setOnLoadCallback(Chartcrustaceosm1);

    function Chartcrustaceosm1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcrustaceosm1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartcrustaceosm1b);

    //Grafica general crustáceos marinos 
    function Chartcrustaceosm1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 643],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcrustaceosm1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartcrustaceosm2);

    //Grafica estimada amenaza crustáceos marinos 
    function Chartcrustaceosm2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcrustaceosm2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartcrustaceosm2b);

    //Grafica SiB amenaza crustáceos marinos 
    function Chartcrustaceosm2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcrustaceosm2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartcrustaceosm3);

    //Grafica estimadas CITES crustáceos marinos 
    function Chartcrustaceosm3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcrustaceosm3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartcrustaceosm3b);

    //Grafica SiB CITES crustáceos marinos 
    function Chartcrustaceosm3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);



        var chart = new google.visualization.PieChart(
          document.getElementById('donutcrustaceosm3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies equinodermos

    google.charts.setOnLoadCallback(Chartequinodermo1);

    function Chartequinodermo1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutequinodermo1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartequinodermo1b);

    //Grafica general equinodermos 
    function Chartequinodermo1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 388],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutequinodermo1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartequinodermo2);

    //Grafica estimada amenaza equinodermos 
    function Chartequinodermo2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutequinodermo2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartequinodermo2b);

    //Grafica SiB amenaza equinodermos 
    function Chartequinodermo2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutequinodermo2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartequinodermo3);

    //Grafica estimadas CITES equinodermos 
    function Chartequinodermo3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 1]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutequinodermo3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartequinodermo3b);

    //Grafica SiB CITES equinodermos 
    function Chartequinodermo3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                1,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 1</br>Registros: 2</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutequinodermo3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies esponja marina

    google.charts.setOnLoadCallback(Chartesponjamarina1);

    function Chartesponjamarina1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutesponjamarina1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartesponjamarina1b);

    //Grafica general esponjas marinas 
    function Chartesponjamarina1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 207],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutesponjamarina1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartesponjamarina2);

    //Grafica estimada amenaza esponjas marinas 
    function Chartesponjamarina2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutesponjamarina2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartesponjamarina2b);

    //Grafica SiB amenaza esponjas marinas 
    function Chartesponjamarina2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutesponjamarina2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartesponjamarina3);

    //Grafica estimadas CITES esponjas marinas 
    function Chartesponjamarina3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutesponjamarina3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartesponjamarina3b);

    //Grafica SiB CITES esponjas marinas 
    function Chartesponjamarina3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutesponjamarina3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies corales afines

    google.charts.setOnLoadCallback(Chartcoralesafines1);

    function Chartcoralesafines1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcoralesafines1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartcoralesafines1b);

    //Grafica general corales 
    function Chartcoralesafines1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 190]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcoralesafines1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartcoralesafines2);

    //Grafica estimada amenaza corales 
    function Chartcoralesafines2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 1],
            ['En peligro', 1],
            ['Vulnerable', 4]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcoralesafines2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartcoralesafines2b);

    //Grafica SiB amenaza corales 
    function Chartcoralesafines2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                1,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 1</br>Registros: 254</div>'
            ],
            [
                'En peligro SiB',
                1,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 1</br>Registros: 418</div>'
            ],
            [
                'Vulnerable SiB',
                4,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 4</br>Registros: 2784</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcoralesafines2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartcoralesafines3);

    //Grafica estimadas CITES corales 
    function Chartcoralesafines3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 123],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcoralesafines3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartcoralesafines3b);

    //Grafica SiB CITES corales 
    function Chartcoralesafines3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                108,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 108</br>Registros: 82.901</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 15, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcoralesafines3b')
        );
        chart.draw(data, options_graph3b);
    }


    // pie de literatura especies medusas

    google.charts.setOnLoadCallback(Chartmedusas1);

    function Chartmedusas1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmedusas1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartmedusas1b);

    //Grafica general medusas 
    function Chartmedusas1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 108],
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmedusas1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartmedusas2);

    //Grafica estimada amenaza medusas 
    function Chartmedusas2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmedusas2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartmedusas2b);

    //Grafica SiB amenaza medusas 
    function Chartmedusas2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmedusas2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartmedusas3);

    //Grafica estimadas CITES medusas 
    function Chartmedusas3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 5],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmedusas3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartmedusas3b);

    //Grafica SiB CITES medusas 
    function Chartmedusas3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                4,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 4</br>Registros: 4.431</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 1, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmedusas3b')
        );
        chart.draw(data, options_graph3b);
    }


    /*----------------------------------------*\
                     PLANTAS
    \*----------------------------------------*/

    // pie de literatura especies angiospermas

    google.charts.setOnLoadCallback(Chartangiospermas1);

    function Chartangiospermas1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutangiospermas1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartangiospermas1b);

    //Grafica general angiospermas 
    function Chartangiospermas1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 31292],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutangiospermas1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartangiospermas2);

    //Grafica estimada amenaza angiospermas 
    function Chartangiospermas2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 104],
            ['En peligro', 255],
            ['Vulnerable', 337]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutangiospermas2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartangiospermas2b);

    //Grafica SiB amenaza angiospermas 
    function Chartangiospermas2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                98,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 98</br>Registros: 3.014</div>'
            ],
            [
                'En peligro SiB',
                236,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 236</br>Registros: 24.963</div>'
            ],
            [
                'Vulnerable SiB',
                303,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 303</br>Registros: 20.534</div>'
            ],
            ['Especies sin datos SiB Colombia', 81, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutangiospermas2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartangiospermas3);

    //Grafica estimadas CITES angiospermas 
    function Chartangiospermas3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 10],
            ['Apéndice II', 2744],
            ['Apéndice III', 1]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutangiospermas3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartangiospermas3b);

    //Grafica SiB CITES angiospermas 
    function Chartangiospermas3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                7,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 7</br>Registros: 164</div>'
            ],
            [
                'Apéndice II en SiB',
                2036,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 2.036</br>Registros: 30.892</div>'
            ],
            [
                'Apéndice III en SiB',
                1,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 1</br>Registros: 103</div>'
            ],
            ['Especies sin datos SiB Colombia', 711, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutangiospermas3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies gimnospermas

    google.charts.setOnLoadCallback(Chartgimnospermas1);

    function Chartgimnospermas1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutgimnospermas1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartgimnospermas1b);

    //Grafica general gimnospermas 
    function Chartgimnospermas1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 112],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutgimnospermas1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartgimnospermas2);

    //Grafica estimada amenaza gimnospermas 
    function Chartgimnospermas2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 8],
            ['En peligro', 9],
            ['Vulnerable', 8]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutgimnospermas2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartgimnospermas2b);

    //Grafica SiB amenaza gimnospermas 
    function Chartgimnospermas2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                8,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 8</br>Registros: 147</div>'
            ],
            [
                'En peligro SiB',
                9,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 9</br>Registros: 505</div>'
            ],
            [
                'Vulnerable SiB',
                8,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 8</br>Registros: 982</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutgimnospermas2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartgimnospermas3);

    //Grafica estimadas CITES gimnospermas 
    function Chartgimnospermas3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 1],
            ['Apéndice II', 24],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutgimnospermas3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartgimnospermas3b);

    //Grafica SiB CITES gimnospermas 
    function Chartgimnospermas3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                1,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 1</br>Registros: 23</div>'
            ],
            [
                'Apéndice II en SiB',
                21,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 21</br>Registros: 821</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 3, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutgimnospermas3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies helechos

    google.charts.setOnLoadCallback(Charthelechos1);

    function Charthelechos1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthelechos1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Charthelechos1b);

    //Grafica general helechos 
    function Charthelechos1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 2108],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthelechos1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Charthelechos2);

    //Grafica estimada amenaza helechos 
    function Charthelechos2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthelechos2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Charthelechos2b);

    //Grafica SiB amenaza helechos 
    function Charthelechos2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthelechos2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Charthelechos3);

    //Grafica estimadas CITES helechos 
    function Charthelechos3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 73],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthelechos3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Charthelechos3b);

    //Grafica SiB CITES helechos 
    function Charthelechos3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                64,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 64</br>Registros: 30.347</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 9, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthelechos3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies musgos

    google.charts.setOnLoadCallback(Chartmusgos1);

    function Chartmusgos1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmusgos1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartmusgos1b);

    //Grafica general musgos 
    function Chartmusgos1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 1574],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmusgos1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartmusgos2);

    //Grafica estimada amenaza musgos 
    function Chartmusgos2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 6],
            ['En peligro', 4],
            ['Vulnerable', 32]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmusgos2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartmusgos2b);

    //Grafica SiB amenaza musgos 
    function Chartmusgos2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                6,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 6</br>Registros: 53</div>'
            ],
            [
                'En peligro SiB',
                3,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 3</br>Registros: 16</div>'
            ],
            [
                'Vulnerable SiB',
                30,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 30</br>Registros: 337</div>'
            ],
            ['Especies sin datos SiB Colombia', 3, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmusgos2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartmusgos3);

    //Grafica estimadas CITES musgos 
    function Chartmusgos3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmusgos3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartmusgos3b);

    //Grafica SiB CITES musgos 
    function Chartmusgos3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmusgos3b')
        );
        chart.draw(data, options_graph3b);
    }


    // pie de literatura especies antocerotas

    google.charts.setOnLoadCallback(Chartantocerotas1);

    function Chartantocerotas1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutantocerotas1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartantocerotas1b);

    //Grafica general antocerotas 
    function Chartantocerotas1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 19],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutantocerotas1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartantocerotas2);

    //Grafica estimada amenaza antocerotas 
    function Chartantocerotas2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutantocerotas2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartantocerotas2b);


    //Grafica SiB amenaza antocerotas 
    function Chartantocerotas2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutantocerotas2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartantocerotas3);

    //Grafica estimadas CITES antocerotas 
    function Chartantocerotas3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutantocerotas3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartantocerotas3b);

    //Grafica SiB CITES antocerotas 
    function Chartantocerotas3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutantocerotas3b')
        );
        chart.draw(data, options_graph3b);
    }


    // pie de literatura especies hepaticas

    google.charts.setOnLoadCallback(Charthepaticas1);

    function Charthepaticas1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthepaticas1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Charthepaticas1b);

    //Grafica general hepáticas 
    function Charthepaticas1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 1014],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthepaticas1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Charthepaticas2);

    //Grafica estimada amenaza hepáticas 
    function Charthepaticas2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 2],
            ['En peligro', 1],
            ['Vulnerable', 48]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthepaticas2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Charthepaticas2b);

    //Grafica SiB amenaza hepáticas 
    function Charthepaticas2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                2,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 2</br>Registros: 5</div>'
            ],
            [
                'En peligro SiB',
                1,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 1</br>Registros: 84</div>'
            ],
            [
                'Vulnerable SiB',
                42,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 42</br>Registros: 555</div>'
            ],
            ['Especies sin datos SiB Colombia', 6, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthepaticas2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Charthepaticas3);

    //Grafica estimadas CITES hepáticas 
    function Charthepaticas3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthepaticas3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Charthepaticas3b);

    //Grafica SiB CITES hepáticas 
    function Charthepaticas3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthepaticas3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies orquideas

    google.charts.setOnLoadCallback(Chartorquideas1);

    function Chartorquideas1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutorquideas1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartorquideas1b);

    //Grafica general orquídeas 
    function Chartorquideas1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 3922],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutorquideas1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartorquideas2);

    //Grafica estimada amenaza orquídeas 
    function Chartorquideas2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 6],
            ['En peligro', 64],
            ['Vulnerable', 136]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutorquideas2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartorquideas2b);

    //Grafica SiB amenaza orquídeas 
    function Chartorquideas2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                6,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 6</br>Registros: 197</div>'
            ],
            [
                'En peligro SiB',
                61,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 61</br>Registros: 801</div>'
            ],
            [
                'Vulnerable SiB',
                125,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 125</br>Registros: 1165</div>'
            ],
            ['Especies sin datos SiB Colombia', 14, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutorquideas2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartorquideas3);

    //Grafica estimadas CITES orquídeas 
    function Chartorquideas3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 10],
            ['Apéndice II', 2964],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutorquideas3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartorquideas3b);

    //Grafica SiB CITES orquídeas 
    function Chartorquideas3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                7,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 7</br>Registros: 164</div>'
            ],
            [
                'Apéndice II en SiB',
                1988,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 1.988</br>Registros: 20.866</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 709, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutorquideas3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies magnolias y afines

    google.charts.setOnLoadCallback(Chartmagnolias1);

    function Chartmagnolias1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmagnolias1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartmagnolias1b);

    //Grafica general magnolias 
    function Chartmagnolias1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 138],
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmagnolias1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartmagnolias2);

    //Grafica estimada amenaza magnolias 
    function Chartmagnolias2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 15],
            ['En peligro', 20],
            ['Vulnerable', 10]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmagnolias2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartmagnolias2b);


    //Grafica SiB amenaza magnolias 
    function Chartmagnolias2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                15,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 15</br>Registros: 251</div>'
            ],
            [
                'En peligro SiB',
                20,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 20</br>Registros: 783</div>'
            ],
            [
                'Vulnerable SiB',
                10,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 10</br>Registros: 6.033</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmagnolias2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartmagnolias3);

    //Grafica estimadas CITES magnolias 
    function Chartmagnolias3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmagnolias3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartmagnolias3b);

    //Grafica SiB CITES magnolias 
    function Chartmagnolias3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmagnolias3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies bromelias

    google.charts.setOnLoadCallback(Chartbromelias1);

    function Chartbromelias1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutbromelias1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartbromelias1b);

    //Grafica general bromelias 
    function Chartbromelias1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 1269],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutbromelias1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartbromelias2);

    //Grafica estimada amenaza bromelias 
    function Chartbromelias2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 47],
            ['En peligro', 94],
            ['Vulnerable', 114]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutbromelias2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartbromelias2b);

    //Grafica SiB amenaza bromelias 
    function Chartbromelias2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                43,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 43</br>Registros: 465</div>'
            ],
            [
                'En peligro SiB',
                83,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 83</br>Registros: 1.212</div>'
            ],
            [
                'Vulnerable SiB',
                96,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 96</br>Registros: 1.862</div>'
            ],
            ['Especies sin datos SiB Colombia', 33, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutbromelias2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartbromelias3);

    //Grafica estimadas CITES bromelias 
    function Chartbromelias3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutbromelias3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartbromelias3b);

    //Grafica SiB CITES bromelias 
    function Chartbromelias3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutbromelias3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies palmas

    google.charts.setOnLoadCallback(Chartpalmas1);

    function Chartpalmas1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpalmas1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartpalmas1b);

    //Grafica general palmas 
    function Chartpalmas1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 370],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpalmas1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartpalmas2);

    //Grafica estimada amenaza palmas 
    function Chartpalmas2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 7],
            ['En peligro', 22],
            ['Vulnerable', 25]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpalmas2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartpalmas2b);

    //Grafica SiB amenaza palmas 
    function Chartpalmas2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                7,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 7</br>Registros: 119</div>'
            ],
            [
                'En peligro SiB',
                22,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 22</br>Registros: 9.010</div>'
            ],
            [
                'Vulnerable SiB',
                24,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 24</br>Registros: 1.877</div>'
            ],
            ['Especies sin datos SiB Colombia', 1, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpalmas2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartpalmas3);

    //Grafica estimadas CITES palmas 
    function Chartpalmas3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpalmas3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartpalmas3b);

    //Grafica SiB CITES palmas 
    function Chartpalmas3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpalmas3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies zamias

    google.charts.setOnLoadCallback(Chartzamias1);

    function Chartzamias1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutzamias1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartzamias1b);

    //Grafica general zamias 
    function Chartzamias1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 30],
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutzamias1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartzamias2);

    //Grafica estimada amenaza zamias 
    function Chartzamias2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 8],
            ['En peligro', 9],
            ['Vulnerable', 4]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutzamias2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartzamias2b);

    //Grafica SiB amenaza zamias 
    function Chartzamias2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                8,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 8</br>Registros: 147</div>'
            ],
            [
                'En peligro SiB',
                9,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 9</br>Registros: 505</div>'
            ],
            [
                'Vulnerable SiB',
                4,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 4</br>Registros: 196</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutzamias2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartzamias3);

    //Grafica estimadas CITES zamias 
    function Chartzamias3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 1],
            ['Apéndice II', 24],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutzamias3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartzamias3b);

    //Grafica SiB CITES zamias 
    function Chartzamias3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                1,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 1</br>Registros: 23</div>'
            ],
            [
                'Apéndice II en SiB',
                21,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 21</br>Registros: 821</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 3, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutzamias3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies frailejones

    google.charts.setOnLoadCallback(Chartfrailejones1);

    function Chartfrailejones1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutfrailejones1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartfrailejones1b);

    //Grafica general frailejones 
    function Chartfrailejones1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 102],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutfrailejones1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartfrailejones2);

    //Grafica estimada amenaza frailejones 
    function Chartfrailejones2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 7],
            ['En peligro', 17],
            ['Vulnerable', 13]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutfrailejones2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartfrailejones2b);

    //Grafica SiB amenaza frailejones 
    function Chartfrailejones2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                6,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 6</br>Registros: 385</div>'
            ],
            [
                'En peligro SiB',
                13,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 13</br>Registros: 1.743</div>'
            ],
            [
                'Vulnerable SiB',
                10,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 10</br>Registros: 687</div>'
            ],
            ['Especies sin datos SiB Colombia', 8, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutfrailejones2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartfrailejones3);

    //Grafica estimadas CITES frailejones 
    function Chartfrailejones3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutfrailejones3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartfrailejones3b);

    //Grafica SiB CITES frailejones 
    function Chartfrailejones3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutfrailejones3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies cactus

    google.charts.setOnLoadCallback(Chartcactus1);

    function Chartcactus1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcactus1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartcactus1b);

    //Grafica general cactus 
    function Chartcactus1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 91],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcactus1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartcactus2);

    //Grafica estimada amenaza cactus 
    function Chartcactus2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcactus2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartcactus2b);

    //Grafica SiB amenaza cactus 
    function Chartcactus2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcactus2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartcactus3);

    //Grafica estimadas CITES cactus 
    function Chartcactus3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 32],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcactus3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartcactus3b);

    //Grafica SiB CITES cactus 
    function Chartcactus3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                28,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 28</br>Registros: 1.655</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 4, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcactus3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies pinos

    google.charts.setOnLoadCallback(Chartpinos1);

    function Chartpinos1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpinos1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartpinos1b);

    //Grafica general pinos 
    function Chartpinos1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 63],
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpinos1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartpinos2);

    //Grafica estimada amenaza pinos 
    function Chartpinos2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpinos2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartpinos2b);

    //Grafica SiB amenaza pinos 
    function Chartpinos2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpinos2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartpinos3);

    //Grafica estimadas CITES pinos 
    function Chartpinos3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpinos3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartpinos3b);

    //Grafica SiB CITES pinos 
    function Chartpinos3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpinos3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies fenerogamas

    google.charts.setOnLoadCallback(Chartfenerogamas1);

    function Chartfenerogamas1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutfenerogamas1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartfenerogamas1b);

    //Grafica general fanerógamas 
    function Chartfenerogamas1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 313],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutfenerogamas1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartfenerogamas2);

    //Grafica estimada amenaza fanerógamas 
    function Chartfenerogamas2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 11],
            ['En peligro', 28],
            ['Vulnerable', 34]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutfenerogamas2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartfenerogamas2b);

    //Grafica SiB amenaza fanerógamas 
    function Chartfenerogamas2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                11,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 11</br>Registros: 312</div>'
            ],
            [
                'En peligro SiB',
                28,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 28</br>Registros: 1.391</div>'
            ],
            [
                'Vulnerable SiB',
                33,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 33</br>Registros: 663</div>'
            ],
            ['Especies sin datos SiB Colombia', 20, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutfenerogamas2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartfenerogamas3);

    //Grafica estimadas CITES fanerógamas 
    function Chartfenerogamas3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutfenerogamas3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartfenerogamas3b);

    //Grafica SiB CITES fanerógamas 
    function Chartfenerogamas3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutfenerogamas3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies pastos marinos

    google.charts.setOnLoadCallback(Chartpastosm1);

    function Chartpastosm1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpastosm1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartpastosm1b);

    //Grafica general pastos marinos 
    function Chartpastosm1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 4],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpastosm1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartpastosm2);

    //Grafica estimada amenaza pastos marinos 
    function Chartpastosm2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpastosm2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartpastosm2b);

    //Grafica SiB amenaza pastos marinos 
    function Chartpastosm2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpastosm2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartpastosm3);

    //Grafica estimadas CITES pastos marinos 
    function Chartpastosm3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpastosm3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartpastosm3b);

    //Grafica SiB CITES pastos marinos 
    function Chartpastosm3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpastosm3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies maderables

    google.charts.setOnLoadCallback(Chartmaderables1);

    function Chartmaderables1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmaderables1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartmaderables1b);

    //Grafica general maderables 
    function Chartmaderables1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 28],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmaderables1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartmaderables2);

    //Grafica estimada amenaza maderables 
    function Chartmaderables2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 8],
            ['En peligro', 11],
            ['Vulnerable', 7]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmaderables2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartmaderables2b);

    //Grafica SiB amenaza maderables 
    function Chartmaderables2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                8,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 8</br>Registros: 1.481</div>'
            ],
            [
                'En peligro SiB',
                11,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 11</br>Registros: 2.395</div>'
            ],
            [
                'Vulnerable SiB',
                7,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 7</br>Registros: 8.983</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmaderables2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartmaderables3);

    //Grafica estimadas CITES maderables 
    function Chartmaderables3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 3],
            ['Apéndice III', 1]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmaderables3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartmaderables3b);


    //Grafica SiB CITES maderables 
    function Chartmaderables3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                3,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 3</br>Registros: 192</div>'
            ],
            [
                'Apéndice III en SiB',
                1,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 1</br>Registros: 103</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmaderables3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies mangles

    google.charts.setOnLoadCallback(Chartmangles1);

    function Chartmangles1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmangles1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartmangles1b);

    //Grafica general manglares 
    function Chartmangles1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 7],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmangles1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartmangles2);

    //Grafica estimada amenaza manglares 
    function Chartmangles2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 1],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmangles2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartmangles2b);

    //Grafica SiB amenaza manglares 
    function Chartmangles2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                1,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 1</br>Registros: 138</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmangles2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartmangles3);

    //Grafica estimadas CITES manglares 
    function Chartmangles3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmangles3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartmangles3b);

    //Grafica SiB CITES manglares 
    function Chartmangles3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: </div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmangles3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies algas marinas

    google.charts.setOnLoadCallback(Chartalgasmarinas1);

    function Chartalgasmarinas1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasmarinas1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartalgasmarinas1b);

    //Grafica general algas 
    function Chartalgasmarinas1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 430],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasmarinas1b')
        );
        chart.draw(data, options_graph1b);
    }


    google.charts.setOnLoadCallback(Chartalgasmarinas2);

    //Grafica estimada amenaza algas 
    function Chartalgasmarinas2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasmarinas2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartalgasmarinas2b);

    //Grafica SiB amenaza algas 
    function Chartalgasmarinas2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasmarinas2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartalgasmarinas3);

    //Grafica estimadas CITES algas 
    function Chartalgasmarinas3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasmarinas3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartalgasmarinas3b);

    //Grafica SiB CITES algas 
    function Chartalgasmarinas3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasmarinas3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies algas dulceacuícolas

    google.charts.setOnLoadCallback(Chartalgasdulceacuicolas1);

    function Chartalgasdulceacuicolas1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 605]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasdulceacuicolas1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartalgasdulceacuicolas1b);

    function Chartalgasdulceacuicolas1b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'SiB');
        data.addColumn({type: 'string', role: 'tooltip'});
        data.addRows([['Sin dato', 100, 'Dato en construcción']]);

        var options = {
            titlePosition: 'none',
            pieHole: 0.9,
            backgroundColor: 'transparent',
            legend: {position: 'none'},
            chartArea: {width: '222', height: '222'},
            pieSliceBorderColor: 'none',
            slices: {0: {color: '#e7e9ed'}},
            sliceVisibilityThreshold: '0',
            fontSize: '12',
            pieSliceText: 'none',
            tooltip: {isHtml: true, textStyle: {color: '#fff'}}
        };

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasdulceacuicolas1b')
        );
        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(Chartalgasdulceacuicolas2);

    function Chartalgasdulceacuicolas2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasdulceacuicolas2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartalgasdulceacuicolas2b);

    function Chartalgasdulceacuicolas2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasdulceacuicolas2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartalgasdulceacuicolas3);

    function Chartalgasdulceacuicolas3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasdulceacuicolas3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartalgasdulceacuicolas3b);

    function Chartalgasdulceacuicolas3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasdulceacuicolas3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies algas terrestres

    google.charts.setOnLoadCallback(Chartalgasterrestres1);

    function Chartalgasterrestres1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 46]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasterrestres1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartalgasterrestres1b);

    function Chartalgasterrestres1b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'SiB');
        data.addColumn({type: 'string', role: 'tooltip'});
        data.addRows([['Sin dato', 100, 'Dato en construcción']]);

        var options = {
            titlePosition: 'none',
            pieHole: 0.9,
            backgroundColor: 'transparent',
            legend: {position: 'none'},
            chartArea: {width: '222', height: '222'},
            pieSliceBorderColor: 'none',
            slices: {0: {color: '#e7e9ed'}},
            sliceVisibilityThreshold: '0',
            fontSize: '12',
            pieSliceText: 'none',
            tooltip: {isHtml: true, textStyle: {color: '#fff'}}
        };

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasterrestres1b')
        );
        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(Chartalgasterrestres2);

    function Chartalgasterrestres2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasterrestres2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartalgasterrestres2b);

    function Chartalgasterrestres2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasterrestres2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartalgasterrestres3);

    function Chartalgasterrestres3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasterrestres3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartalgasterrestres3b);

    function Chartalgasterrestres3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutalgasterrestres3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies otras algas

    google.charts.setOnLoadCallback(Chartotrasalgas1);

    function Chartotrasalgas1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 28]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutotrasalgas1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartotrasalgas1b);

    function Chartotrasalgas1b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'SiB');
        data.addColumn({type: 'string', role: 'tooltip'});
        data.addRows([['Sin dato', 0, 'Dato en construcción']]);

        var options = {
            titlePosition: 'none',
            pieHole: 0.9,
            backgroundColor: 'transparent',
            legend: {position: 'none'},
            chartArea: {width: '222', height: '222'},
            pieSliceBorderColor: 'none',
            slices: {0: {color: '#e7e9ed'}},
            sliceVisibilityThreshold: '0',
            fontSize: '12',
            pieSliceText: 'none',
            tooltip: {isHtml: true, textStyle: {color: '#fff'}}
        };

        var chart = new google.visualization.PieChart(
          document.getElementById('donutotrasalgas1b')
        );
        chart.draw(data, options);
    }

    google.charts.setOnLoadCallback(Chartotrasalgas2);

    function Chartotrasalgas2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutotrasalgas2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartotrasalgas2b);

    function Chartotrasalgas2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutotrasalgas2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartotrasalgas3);

    function Chartotrasalgas3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutotrasalgas3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartotrasalgas3b);

    function Chartotrasalgas3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutotrasalgas3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies liquenes

    google.charts.setOnLoadCallback(Chartliquenes1);

    function Chartliquenes1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutliquenes1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartliquenes1b);

    //Grafica general líquenes
    function Chartliquenes1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 1803],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutliquenes1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartliquenes2);

    //Grafica estimada amenaza líquenes 
    function Chartliquenes2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutliquenes2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartliquenes2b);

    //Grafica SiB amenaza líquenes 
    function Chartliquenes2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutliquenes2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartliquenes3);

    //Grafica estimadas CITES líquenes 
    function Chartliquenes3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutliquenes3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartliquenes3b);

    //Grafica SiB CITES líquenes
    function Chartliquenes3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutliquenes3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies macrohongos

    google.charts.setOnLoadCallback(Chartmacrohongos1);

    function Chartmacrohongos1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmacrohongos1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartmacrohongos1b);

    //Grafica general hongos 
    function Chartmacrohongos1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 4056],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmacrohongos1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartmacrohongos2);

    //Grafica estimada amenaza hongos 
    function Chartmacrohongos2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmacrohongos2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartmacrohongos2b);

    //Grafica SiB amenaza hongos 
    function Chartmacrohongos2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmacrohongos2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartmacrohongos3);

    //Grafica estimadas CITES hongos 
    function Chartmacrohongos3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmacrohongos3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartmacrohongos3b);

    //Grafica SiB CITES hongos 
    function Chartmacrohongos3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmacrohongos3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies royas

    google.charts.setOnLoadCallback(Chartroyas1);

    function Chartroyas1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 456]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutroyas1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartroyas1b);

    function Chartroyas1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 0],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutroyas1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartroyas2);

    function Chartroyas2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutroyas2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartroyas2b);

    function Chartroyas2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutroyas2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartroyas3);

    function Chartroyas3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutroyas3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartroyas3b);

    function Chartroyas3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutroyas3b')
        );
        chart.draw(data, options_graph3b);
    }

    // pie de literatura especies carbones

    google.charts.setOnLoadCallback(Chartcarbones1);

    function Chartcarbones1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 71]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcarbones1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(Chartcarbones1b);

    function Chartcarbones1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 0],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcarbones1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(Chartcarbones2);

    function Chartcarbones2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcarbones2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(Chartcarbones2b);

    function Chartcarbones2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcarbones2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(Chartcarbones3);

    function Chartcarbones3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcarbones3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartcarbones3b);

    function Chartcarbones3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutcarbones3b')
        );
        chart.draw(data, options_graph3b);
    }
}

//final graficas

//funciones graficas de mamiferos tercer nivel
$('.mamiferosback').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.mamiferos').css('display', 'block');
});

$('.mamiferosm').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.mamiferosmgraph').css('display', 'block');


    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(Chartmama1);

    function Chartmama1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmama1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(Chartmama1b);

    //Grafica general mamíferos marinos 
    function Chartmama1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 23],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmama1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(Chartmama2);

    //Grafica estimada amenaza mamíferos marinos 
    function Chartmama2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 4],
            ['Vulnerable', 2]
        ]);



        var chart = new google.visualization.PieChart(
          document.getElementById('donutmama2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(Chartmama2b);

    //Grafica SiB amenaza mamíferos marinos 
    function Chartmama2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                3,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 3</br>Registros: 179</div>'
            ],
            [
                'Vulnerable SiB',
                2,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 2</br>Registros: 1442</div>'
            ],
            ['Especies sin datos SiB Colombia', 1, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmama2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(ChartMama3);

    //Grafica estimadas CITES mamíferos marinos 
    function ChartMama3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 9],
            ['Apéndice II', 20],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmama3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(ChartMama3b);

    //Grafica SiB CITES mamíferos marinos 
    function ChartMama3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                7,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 7</br>Registros: 1626</div>'
            ],
            [
                'Apéndice II en SiB',
                16,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 16</br>Registros: 264</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 6, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmama3b')
        );
        chart.draw(data, options_graph3b);
    }
});

//funciones graficas de reptiles tercer nivel
$('.reptilesback').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.reptiles').css('display', 'block');
});

$('.reptilesm').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.reptilesmgraph').css('display', 'block');


    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(ChartReptilm1);

    function ChartReptilm1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutreptilm1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(ChartReptil1b);

    //Grafica general tortugas marinas 
    function ChartReptil1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 6],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutreptilm1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(ChartReptilm2);

    //Grafica estimada amenaza tortugas marinas 
    function ChartReptilm2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 3],
            ['En peligro', 1],
            ['Vulnerable', 1]
        ]);



        var chart = new google.visualization.PieChart(
          document.getElementById('donutreptilm2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(ChartReptilm2b);

    //Grafica SiB amenaza tortugas marinas 
    function ChartReptilm2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                3,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 3</br>Registros: 2.456</div>'
            ],
            [
                'En peligro SiB',
                1,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 1</br>Registros: 702</div>'
            ],
            [
                'Vulnerable SiB',
                1,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 1</br>Registros: 240</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutreptilm2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(ChartMama3);

    //Grafica estimadas CITES tortugas marinas 
    function ChartMama3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 6],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutreptilm3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(ChartReptilm3b);

    //Grafica SiB CITES tortugas marinas 
    function ChartReptilm3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                5,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 5</br>Registros: 3.398</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 1, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutreptilm3b')
        );
        chart.draw(data, options_graph3b);
    }
});

//funciones graficas de peces tercer nivel

$('.pecesback').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.peces').css('display', 'block');
});

$('.pecesm').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.pecesmgraph').css('display', 'block');

    // pie de literatura especies peces marinos

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(Chartpecesm1);

    function Chartpecesm1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpecesm1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(Chartpecesm1b);

    //Grafica general peces marinos 
    function Chartpecesm1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 873],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpecesm1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(Chartpecesm2);

    //Grafica estimada amenaza peces marinos 
    function Chartpecesm2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 1],
            ['En peligro', 2],
            ['Vulnerable', 9]
        ]);



        var chart = new google.visualization.PieChart(
          document.getElementById('donutpecesm2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(Chartpecesm2b);

    //Grafica SiB amenaza peces marinos 
    function Chartpecesm2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                2,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 2</br>Registros: 80</div>'
            ],
            [
                'Vulnerable SiB',
                8,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 8</br>Registros: 77</div>'
            ],
            ['Especies sin datos SiB Colombia', 2, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpecesm2b')
        );
        chart.draw(data, options_graph2b);
    }
    google.charts.setOnLoadCallback(Chartpecesm3);

    //Grafica estimadas CITES peces marinos 
    function Chartpecesm3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 4],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpecesm3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartpecesm3b);

    //Grafica SiB CITES peces marinos 
    function Chartpecesm3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                4,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 4</br>Registros: 27</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpecesm3b')
        );
        chart.draw(data, options_graph3b);
    }
});

$('.pecesd').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.pecesdgraph').css('display', 'block');

    // pie de literatura especies peces dulces

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(Chartpecesd1);

    function Chartpecesd1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 1.616]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpecesd1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(Chartpecesd1b);
    
    //Grafica general peces dulceacuícolas 
    function Chartpecesd1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 1458],
            ['Especies sin datos SiB Colombia', 158 ]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpecesd1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(Chartpecesd2);

    //Grafica estimada amenaza peces dulceacuícolas 
    function Chartpecesd2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 2],
            ['En peligro', 3],
            ['Vulnerable', 47]
        ]);



        var chart = new google.visualization.PieChart(
          document.getElementById('donutpecesd2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(Chartpecesd2b);

    //Grafica SiB amenaza peces dulceacuícolas 
    function Chartpecesd2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                2,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 2</br>Registros: 2.093</div>'
            ],
            [
                'En peligro SiB',
                3,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 3</br>Registros: 203</div>'
            ],
            [
                'Vulnerable SiB',
                47,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 47</br>Registros: 4.403</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpecesd2b')
        );
        chart.draw(data, options_graph2b);
    }


    google.charts.setOnLoadCallback(Chartpecesd3);

    //Grafica estimadas CITES peces dulceacuícolas 
    function Chartpecesd3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 1],
            ['Apéndice III', 6]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpecesd3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(Chartpecesd3b);

    //Grafica SiB CITES peces dulceacuícolas 
    function Chartpecesd3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                1,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 1</br>Registros: 5</div>'
            ],
            [
                'Apéndice III en SiB',
                6,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 6</br>Registros: 757</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutpecesd3b')
        );
        chart.draw(data, options_graph3b);
    }
});

//funciones graficas de insectos tercer nivel

$('.insectosback').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.insectos').css('display', 'block');
});

$('.escarabajos').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.graph_escarabajos').css('display', 'block');

    // pie de literatura especies escarabajos
    google.charts.setOnLoadCallback(ChartinsectosEscarabajos1);

    function ChartinsectosEscarabajos1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutescarabajos1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(ChartinsectosEscarabajos1b);

    //Grafica general escarabajos 
    function ChartinsectosEscarabajos1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 2451],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutescarabajos1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(ChartinsectosEscarabajos2);

    //Grafica estimada amenaza escarabajos 
    function ChartinsectosEscarabajos2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 1],
            ['Vulnerable', 4]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutescarabajos2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(ChartinsectosEscarabajos2b);

    //Grafica SiB amenaza escarabajos 
    function ChartinsectosEscarabajos2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                1,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 1</br>Registros: 32</div>'
            ],
            [
                'Vulnerable SiB',
                2,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 2</br>Registros: 100</div>'
            ],
            ['Especies sin datos SiB Colombia', 2, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutescarabajos2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(ChartinsectosEscarabajos3);

    //Grafica estimadas CITES escarabajos 
    function ChartinsectosEscarabajos3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutescarabajos3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(ChartinsectosEscarabajos3b);

    //Grafica SiB CITES escarabajos 
    function ChartinsectosEscarabajos3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutescarabajos3b')
        );
        chart.draw(data, options_graph3b);
    }
});

$('.mariposas').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.graph_mariposas').css('display', 'block');

    // pie de literatura especies mariposas
    google.charts.setOnLoadCallback(ChartinsectosMariposas1);

    function ChartinsectosMariposas1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmariposas1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(ChartinsectosMariposas1b);

    //Grafica general mariposas 
    function ChartinsectosMariposas1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 4742],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmariposas1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(ChartinsectosMariposas2);

    //Grafica estimada amenaza mariposas 
    function ChartinsectosMariposas2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 1],
            ['En peligro', 6],
            ['Vulnerable', 5]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmariposas2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(ChartinsectosMariposas2b);

    //Grafica SiB amenaza mariposas 
    function ChartinsectosMariposas2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                1,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 1</br>Registros: 8</div>'
            ],
            [
                'En peligro SiB',
                4,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 4</br>Registros: 82</div>'
            ],
            [
                'Vulnerable SiB',
                4,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 4</br>Registros: 40</div>'
            ],
            ['Especies sin datos SiB Colombia', 3, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmariposas2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(ChartinsectosMariposas3);

    //Grafica estimadas CITES mariposas 
    function ChartinsectosMariposas3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmariposas3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(ChartinsectosMariposas3b);

    //Grafica SiB CITES mariposas 
    function ChartinsectosMariposas3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmariposas3b')
        );
        chart.draw(data, options_graph3b);
    }
});

$('.hormigas').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.graph_hormigas').css('display', 'block');

    // pie de literatura especies hormigas
    google.charts.setOnLoadCallback(ChartinsectosHormigas1);

    function ChartinsectosHormigas1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthormigas1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(ChartinsectosHormigas1b);

    //Grafica general hormigas 
    function ChartinsectosHormigas1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 1012],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthormigas1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(ChartinsectosHormigas2);

    //Grafica estimada amenaza hormigas 
    function ChartinsectosHormigas2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 2]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthormigas2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(ChartinsectosHormigas2b);

    //Grafica SiB amenaza hormigas 
    function ChartinsectosHormigas2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                2,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 2</br>Registros: 4</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthormigas2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(ChartinsectosHormigas3);

    //Grafica estimadas CITES hormigas 
    function ChartinsectosHormigas3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthormigas3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(ChartinsectosHormigas3b);

    //Grafica SiB CITES hormigas 
    function ChartinsectosHormigas3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donuthormigas3b')
        );
        chart.draw(data, options_graph3b);
    }
});

$('.abejas').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.graph_abejas').css('display', 'block');

    // pie de literatura especies abejas
    google.charts.setOnLoadCallback(ChartinsectosAbejas1);

    function ChartinsectosAbejas1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutabejas1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(ChartinsectosAbejas1b);

    //Grafica general abejas 
    function ChartinsectosAbejas1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 400],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutabejas1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(ChartinsectosAbejas2);

   //Grafica estimada amenaza abejas 
    function ChartinsectosAbejas2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 4],
            ['En peligro', 4],
            ['Vulnerable', 3]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutabejas2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(ChartinsectosAbejas2b);

    //Grafica SiB amenaza abejas 
    function ChartinsectosAbejas2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                3,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 3</br>Registros: 87</div>'
            ],
            [
                'En peligro SiB',
                3,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 3</br>Registros: 10</div>'
            ],
            [
                'Vulnerable SiB',
                3,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 3</br>Registros: 227</div>'
            ],
            ['Especies sin datos SiB Colombia', 2, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutabejas2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(ChartinsectosAbejas3);

    //Grafica estimadas CITES abejas 
    function ChartinsectosAbejas3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutabejas3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(ChartinsectosAbejas3b);

    //Grafica SiB CITES abejas 
    function ChartinsectosAbejas3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutabejas3b')
        );
        chart.draw(data, options_graph3b);
    }
});

$('.dipteros').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.graph_dipteros').css('display', 'block');

    // pie de literatura especies dipteros
    google.charts.setOnLoadCallback(ChartinsectosDipteros1);

    function ChartinsectosDipteros1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdipteros1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(ChartinsectosDipteros1b);

    //Grafica general dípteros 
    function ChartinsectosDipteros1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 1743],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdipteros1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(ChartinsectosDipteros2);

    //Grafica estimada amenaza dípteros 
    function ChartinsectosDipteros2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdipteros2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(ChartinsectosDipteros2b);

    //Grafica SiB amenaza dípteros 
    function ChartinsectosDipteros2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdipteros2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(ChartinsectosDipteros3);

    //Grafica estimadas CITES dípteros 
    function ChartinsectosDipteros3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdipteros3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(ChartinsectosDipteros3b);

    //Grafica SiB CITES dípteros 
    function ChartinsectosDipteros3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdipteros3b')
        );
        chart.draw(data, options_graph3b);
    }
});

//funciones graficas de moluscos tercer nivel

$('.moluscosback').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.moluscos').css('display', 'block');
});

$('.marinos').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.graph_marinos').css('display', 'block');

    // pie de literatura especies moluscos marinos
    google.charts.setOnLoadCallback(ChartmoluscosMarinos1);

    function ChartmoluscosMarinos1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmarinos1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(ChartmoluscosMarinos1b);

    //Grafica general moluscos marinos
    function ChartmoluscosMarinos1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 1192],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmarinos1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(ChartmoluscosMarinos2);

    //Grafica estimada amenaza moluscos marinos  
    function ChartmoluscosMarinos2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmarinos2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(ChartmoluscosMarinos2b);

    //Grafica SiB amenaza moluscos marinos 
    function ChartmoluscosMarinos2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmarinos2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(ChartmoluscosMarinos3);

    //Grafica estimadas CITES moluscos marinos  
    function ChartmoluscosMarinos3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmarinos3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(ChartmoluscosMarinos3b);

    //Grafica SiB CITES moluscos marinos
    function ChartmoluscosMarinos3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutmarinos3b')
        );
        chart.draw(data, options_graph3b);
    }
});

$('.terrestres').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.graph_terrestres').css('display', 'block');

    // pie de literatura especies moluscos terrestres
    google.charts.setOnLoadCallback(ChartmoluscosTerrestres1);

    function ChartmoluscosTerrestres1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 650]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutterrestres1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(ChartmoluscosTerrestres1b);

    function ChartmoluscosTerrestres1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 0],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutterrestres1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(ChartmoluscosTerrestres2);

    function ChartmoluscosTerrestres2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutterrestres2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(ChartmoluscosTerrestres2b);

    function ChartmoluscosTerrestres2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutterrestres2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(ChartmoluscosTerrestres3);

    function ChartmoluscosTerrestres3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutterrestres3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(ChartmoluscosTerrestres3b);

    function ChartmoluscosTerrestres3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutterrestres3b')
        );
        chart.draw(data, options_graph3b);
    }
});

//funciones graficas de decápodos tercer nivel

$('.decapodosback').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.decapodos').css('display', 'block');
});

$('.decapodos_marinos').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.graph_decapodos_marinos').css('display', 'block');

    // pie de literatura especies decapodos marinos
    google.charts.setOnLoadCallback(ChartdecapodosMarinos1);

    function ChartdecapodosMarinos1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosmarinos1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(ChartdecapodosMarinos1b);

    function ChartdecapodosMarinos1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 0],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosmarinos1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(ChartdecapodosMarinos2);

    function ChartdecapodosMarinos2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 7]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosmarinos2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(ChartdecapodosMarinos2b);

    function ChartdecapodosMarinos2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                3,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 3</br>Registros: 16</div>'
            ],
            ['Especies sin datos SiB Colombia', 4, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosmarinos2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(ChartdecapodosMarinos3);

    function ChartdecapodosMarinos3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosmarinos3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(ChartdecapodosMarinos3b);

    function ChartdecapodosMarinos3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosmarinos3b')
        );
        chart.draw(data, options_graph3b);
    }
});

$('.decapodos_terrestres').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.graph_decapodos_terrestres').css('display', 'block');

    // pie de literatura especies decapodos terrestres
    google.charts.setOnLoadCallback(ChartdecapodosTerrestres1);

    function ChartdecapodosTerrestres1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosterrestres1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(ChartdecapodosTerrestres1b);

    function ChartdecapodosTerrestres1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 0],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosterrestres1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(ChartdecapodosTerrestres2);

    function ChartdecapodosTerrestres2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 0],
            ['Vulnerable', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosterrestres2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(ChartdecapodosTerrestres2b);

    function ChartdecapodosTerrestres2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                0,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosterrestres2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(ChartdecapodosTerrestres3);

    function ChartdecapodosTerrestres3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosterrestres3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(ChartdecapodosTerrestres3b);

    function ChartdecapodosTerrestres3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosterrestres3b')
        );
        chart.draw(data, options_graph3b);
    }
});

$('.decapodos_dulceacuicolas').on('click', function () {
    $('.grapharea').css('display', 'none');
    $('.graph_decapodos_dulceacuicolas').css('display', 'block');

    // pie de literatura especies decapodos dulce
    google.charts.setOnLoadCallback(ChartdecapodosDulce1);

    function ChartdecapodosDulce1() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Representatividad de especies', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosdulce1')
        );
        chart.draw(data, options_graph1);
    }

    google.charts.setOnLoadCallback(ChartdecapodosDulce1b);

    function ChartdecapodosDulce1b() {
        var data = google.visualization.arrayToDataTable([
            ['Titulo', 'SiB'],
            ['Especies con evidencia en el SiB Colombia', 0],
            ['Especies sin datos SiB Colombia', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosdulce1b')
        );
        chart.draw(data, options_graph1b);
    }

    google.charts.setOnLoadCallback(ChartdecapodosDulce2);

    function ChartdecapodosDulce2() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['En peligro crítico', 0],
            ['En peligro', 25],
            ['Vulnerable', 1]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosdulce2')
        );
        chart.draw(data, options_graph2);
    }

    google.charts.setOnLoadCallback(ChartdecapodosDulce2b);

    function ChartdecapodosDulce2b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'En peligro crítico SiB',
                0,
                '<div class="pad10"><b>En peligro crítico SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'En peligro SiB',
                6,
                '<div class="pad10"><b>En peligro SiB Colombia</b></br>Especies: 6</br>Registros: 12</div>'
            ],
            [
                'Vulnerable SiB',
                0,
                '<div class="pad10"><b>Vulnerable SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 20, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosdulce2b')
        );
        chart.draw(data, options_graph2b);
    }

    google.charts.setOnLoadCallback(ChartdecapodosDulce3);

    function ChartdecapodosDulce3() {
        var data = google.visualization.arrayToDataTable([
            ['Especies estimadas', 'Número de especies'],
            ['Apéndice I', 0],
            ['Apéndice II', 0],
            ['Apéndice III', 0]
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosdulce3')
        );
        chart.draw(data, options_graph3);
    }

    google.charts.setOnLoadCallback(ChartdecapodosDulce3b);

    function ChartdecapodosDulce3b() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Título');
        data.addColumn('number', 'Especies');
        data.addColumn({type: 'string', role: 'tooltip', p: {html: true}});
        data.addRows([
            [
                'Apéndice I en SiB',
                0,
                '<div class="pad10"><b>Apéndice I en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice II en SiB',
                0,
                '<div class="pad10"><b>Apéndice II en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            [
                'Apéndice III en SiB',
                0,
                '<div class="pad10"><b>Apéndice III en SiB Colombia</b></br>Especies: 0</br>Registros: 0</div>'
            ],
            ['Especies sin datos SiB Colombia', 0, '']
        ]);

        var chart = new google.visualization.PieChart(
          document.getElementById('donutdecapodosdulce3b')
        );
        chart.draw(data, options_graph3b);
    }
});

//show hide otras graficas

function showDivspp1() {
    document.getElementById('chart_div_colecciones').style.display = 'none';
    document.getElementById('chart_div_observaciones').style.display = 'none';
    document.getElementById('chart_div_records').style.display = 'block';
}

function showDivspp2() {
    document.getElementById('chart_div_observaciones').style.display = 'none';
    document.getElementById('chart_div_records').style.display = 'none';
    document.getElementById('chart_div_colecciones').style.display = 'block';

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(drawChartColecciones);

    function drawChartColecciones() {
        var data = google.visualization.arrayToDataTable([
            ['Fecha', 'Plantas', 'Animales', 'Hongos', 'Otros'],
            ['Dic.07', 19471, 5591, 1333, 92],
            ['Abr.08', 19666, 5641, 1378, 92],
            ['Jun.08', 22163, 6016, 1426, 113],
            ['Oct.08', 22180, 7780, 1433, 118],
            ['Dic.08', 22527, 7764, 1463, 119],
            ['Abr.09', 23112, 8040, 1475, 119],
            ['Jun.09', 23110, 8429, 1481, 119],
            ['Sept.09', 24057, 9891, 1625, 122],
            ['Dic.09', 24407, 11309, 1626, 122],
            ['Abr.10', 24466, 11315, 1639, 122],
            ['Jul.10', 24325, 11110, 1393, 113],
            ['Nov.10', 25003, 11501, 1683, 133],
            ['Feb.11', 24890, 12600, 1694, 135],
            ['Jun.11', 24895, 13177, 1705, 120],
            ['Sept.11', 25302, 13178, 1700, 120],
            ['Ene.12', 25120, 13725, 1687, 120],
            ['Mar.12', 25584, 13618, 1696, 120],
            ['Jul.12', 25647, 14271, 1707, 124],
            ['Oct.12', 26766, 14914, 1844, 128],
            ['Dic.12', 26985, 14911, 1852, 129],
            ['Feb.13', 27983, 14970, 1924, 129],
            ['May.13', 28831, 15888, 1927, 129],
            ['Jul.13', 28927, 15992, 1943, 129],
            ['Sept.13', 29010, 15979, 1943, 129],
            ['Dic.13', 29069, 15888, 1937, 128],
            ['Mar.14', 29259, 16881, 2020, 190],
            ['Sept.14', 28392, 15465, 2046, 167],
            ['Ene.15', 28806, 15920, 2074, 173],
            ['Abr.15', 29310, 16808, 2110, 198],
            ['Jul.15', 29945, 17197, 2214, 205],
            ['Oct.15', 31403, 20480, 2239, 276],
            ['Ene.16', 31654, 20636, 2254, 276],
            ['Abr.16', 32548, 21344, 2292, 276],
            ['Jul.16', 32596, 21595, 2309, 277],
            ['Oct.16', 32640, 21743, 2327, 282],
            ['Dic.16', 33767, 21969, 2342, 288],
            ['Abr.17', 33238, 22702, 2333, 272],
            ['Jul.17', 33810, 23199, 2357, 294],
            ['Oct.17', 33602, 23373, 2399, 272]
        ]);

        var options_chart = {
            legend: 'none',
            hAxis: {
                title: 'Fecha',
                titleTextStyle: {color: '#6f6f6f'},
                gridlines: {color: '#fff', count: 10},
                textStyle: {color: '#6f6f6f', fontSize: 12}
            },
            vAxis: {
                title: 'Número especies observadas (en miles)',
                titleTextStyle: {color: '#6f6f6f'},
                minValue: 0,
                gridlines: {color: '#fff'},
                format: 'short',
                textStyle: {color: '#6f6f6f', fontSize: 12}
            },
            isStacked: true,
            areaOpacity: 0.7,
            chartArea: {
                backgroundColor: '#E7E9ED',
                left: 70,
                top: 20,
                right: 50,
                width: '100%',
                height: 400
            },
            colors: ['#9A9ACC', '#4BC0C0', '#FF6383', '#FFCE56'],
            width: '100%'
        };

        var chart = new google.visualization.AreaChart(
          document.getElementById('chart_div_colecciones')
        );
        chart.draw(data, options_chart);
    }
}

function showDivspp3() {
    document.getElementById('chart_div_records').style.display = 'none';
    document.getElementById('chart_div_colecciones').style.display = 'none';
    document.getElementById('chart_div_observaciones').style.display = 'block';

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(drawChartObservaciones);

    function drawChartObservaciones() {
        var data = google.visualization.arrayToDataTable([
            ['Fecha', 'Plantas', 'Animales', 'Hongos', 'Otros'],
            ['Dic.07', 0, 67, 19, 0],
            ['Abr.08', 0, 70, 19, 0],
            ['Jun.08', 0, 432, 19, 32],
            ['Oct.08', 0, 458, 20, 75],
            ['Dic.08', 0, 848, 20, 80],
            ['Abr.09', 0, 862, 20, 80],
            ['Jun.09', 0, 905, 20, 80],
            ['Sept.09', 146, 906, 22, 80],
            ['Dic.09', 148, 918, 22, 80],
            ['Abr.10', 148, 919, 22, 95],
            ['Jul.10', 148, 919, 22, 95],
            ['Nov.10', 201, 1604, 22, 95],
            ['Feb.11', 2455, 2878, 31, 100],
            ['Jun.11', 2783, 3457, 31, 108],
            ['Sept.11', 2783, 3458, 31, 108],
            ['Ene.12', 2781, 3463, 31, 108],
            ['Mar.12', 2781, 3447, 31, 108],
            ['Jul.12', 2843, 3589, 31, 108],
            ['Oct.12', 2843, 3602, 32, 108],
            ['Dic.12', 2844, 3743, 32, 108],
            ['Feb.13', 2323, 2823, 32, 108],
            ['May.13', 3120, 2823, 113, 108],
            ['Jul.13', 3293, 2838, 116, 108],
            ['Sept.13', 3294, 2913, 116, 108],
            ['Dic.13', 3344, 3146, 118, 108],
            ['Mar.14', 3351, 3192, 119, 108],
            ['Sept.14', 3569, 3247, 115, 108],
            ['Ene.15', 3790, 3334, 115, 108],
            ['Abr.15', 3977, 3390, 115, 108],
            ['Jul.15', 4080, 3460, 117, 116],
            ['Oct.15', 4199, 3460, 116, 116],
            ['Ene.16', 4232, 3530, 117, 228],
            ['Abr.16', 4593, 3712, 117, 228],
            ['Jul.16', 4815, 3835, 117, 228],
            ['Oct.16', 4835, 3960, 118, 228],
            ['Dic.16', 4861, 4110, 118, 231],
            ['Abr.17', 5055, 4208, 120, 237],
            ['Jul.17', 5207, 4317, 124, 237],
            ['Oct.17', 5318, 4397, 131, 237]
        ]);

        var options_chart = {
            legend: 'none',
            hAxis: {
                title: 'Fecha',
                titleTextStyle: {color: '#6f6f6f'},
                gridlines: {color: '#fff', count: 10},
                textStyle: {color: '#6f6f6f', fontSize: 12}
            },
            vAxis: {
                title: 'Número especies observadas (en miles)',
                titleTextStyle: {color: '#6f6f6f'},
                minValue: 0,
                gridlines: {color: '#fff'},
                format: 'short',
                textStyle: {color: '#6f6f6f', fontSize: 12}
            },
            isStacked: true,
            areaOpacity: 0.7,
            chartArea: {
                backgroundColor: '#E7E9ED',
                left: 70,
                top: 20,
                right: 50,
                width: '100%',
                height: 400
            },
            colors: ['#9A9ACC', '#4BC0C0', '#FF6383', '#FFCE56'],
            width: '100%'
        };

        var chart = new google.visualization.AreaChart(
          document.getElementById('chart_div_observaciones')
        );
        chart.draw(data, options_chart);
    }
}

function showDivmap1() {
    document.getElementById('mapa2').style.display = 'none';
    document.getElementById('mapa1').style.display = 'block';
}

function showDivmap2() {
    document.getElementById('mapa1').style.display = 'none';
    document.getElementById('mapa2').style.display = 'block';

    //Mapa departamentos registros

    google.charts.load('current', {packages: ['geochart']});
    google.charts.setOnLoadCallback(drawMarkersMap2);

    function drawMarkersMap2() {
        var data = google.visualization.arrayToDataTable([
            ['Departament', 'Nombre', 'Registros'],
            ['CO-DC', 'Bogotá Distrito Capital', 307336],
            ['CO-AMA', 'Amazonas', 230505],
            ['CO-ANT', 'Antioquia', 1529974],
            ['CO-ARA', 'Arauca', 69358],
            ['CO-ATL', 'Atlántico', 85788],
            ['CO-BOL', 'Bolívar', 365508],
            ['CO-BOY', 'Boyacá', 427815],
            ['CO-CAL', 'Caldas', 1040822],
            ['CO-CAQ', 'Caquetá', 283373],
            ['CO-CAS', 'Casanare', 266176],
            ['CO-CAU', 'Cauca', 301848],
            ['CO-CES', 'Cesar', 101495],
            ['CO-COR', 'Córdoba', 145509],
            ['CO-CUN', 'Cundinamarca', 1110577],
            ['CO-CHO', 'Chocó', 383284],
            ['CO-GUA', 'Guainía', 103629],
            ['CO-GUV', 'Guaviare', 74104],
            ['CO-HUI', 'Huila', 236910],
            ['CO-LAG', 'La Guajira', 321291],
            ['CO-MAG', 'Magdalena', 808798],
            ['CO-MET', 'Meta', 803405],
            ['CO-NAR', 'Nariño', 637221],
            ['CO-NSA', 'Norte de Santander', 71237],
            ['CO-PUT', 'Putumayo', 262163],
            ['CO-QUI', 'Quindío', 273203],
            ['CO-RIS', 'Risaralda', 672696],
            ['CO-ARC', 'San Andrés, Providencia y Santa Catalina', 111331],
            ['CO-SAN', 'Santander', 493137],
            ['CO-SUC', 'Sucre', 108733],
            ['CO-TOL', 'Tolima', 609259],
            ['CO-VAC', 'Valle del Cauca', 1473130],
            ['CO-VAU', 'Vaupés', 149825],
            ['CO-VID', 'Vichada', 95603]
        ]);

        var options = {
            region: 'CO',
            resolution: 'provinces',
            displayMode: 'regions',
            enableRegionInteractivity: true,
            backgroundColor: 'transparent',
            colors: ['#F7E2AD', '#F7B000'],
            datalessRegionColor: 'transparent',
            width: '100%',
            tooltip: {
                isHtml: true,
                textStyle: {color: '#fff', fontName: 'Open Sans'}
            }
        };

        var chart = new google.visualization.GeoChart(
          document.getElementById('chart_div2')
        );
        chart.draw(data, options);
    }
}

//graficas que cargan al inicio

$(document).ready(function () {
    // Tooltip only Text

    $('.masterTooltip')
      .hover(
        function () {
            var title = $(this).attr('title');
            $(this)
              .data('tipText', title)
              .removeAttr('title');
            $('<p class="tooltip"></p>')
              .text(title)
              .appendTo('body')
              .fadeIn('fast');
        },
        function () {
            $(this).attr('title', $(this).data('tipText'));
            $('.tooltip').remove();
        }
      )
      .mousemove(function (e) {
          var mousex = e.pageX + 20;
          var mousey = e.pageY + 10;
          $('.tooltip').css({top: mousey, left: mousex});
      });

    $('.masterTooltip2')
      .hover(
        function () {
            var title = $(this).attr('title');
            $(this)
              .data('tipText', title)
              .removeAttr('title');
            $('<p class="tooltip2"></p>')
              .text(title)
              .appendTo('body')
              .fadeIn('fast');
        },
        function () {
            $(this).attr('title', $(this).data('tipText'));
            $('.tooltip2').remove();
        }
      )
      .mousemove(function (e) {
          var mousex = e.pageX + 20;
          var mousey = e.pageY + 10;
          $('.tooltip2').css({top: mousey, left: mousex});
      });

    //primer grafica de especies

    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(drawChartRegisters);

    function drawChartRegisters() {
        var data = google.visualization.arrayToDataTable([
            ['Fecha', 'Plantas', 'Animales', 'Hongos', 'Otros'],
            ['Dic.07', 20886, 6885, 1554, 154],
            ['Abr.08', 21051, 8158, 1569, 154],
            ['Jun.08', 23157, 8897, 1616, 190],
            ['Oct.08', 23181, 8994, 1622, 233],
            ['Dic.08', 23484, 9125, 1652, 239],
            ['Abr.09', 24065, 9330, 1666, 242],
            ['Jun.09', 24072, 9658, 1672, 242],
            ['Sept.09', 24951, 11129, 1813, 243],
            ['Dic.09', 25277, 12568, 1814, 243],
            ['Abr.10', 25335, 12575, 1827, 258],
            ['Jul.10', 25182, 12304, 1582, 244],
            ['Nov.10', 25809, 12948, 1870, 277],
            ['Feb.11', 26473, 14594, 1888, 284],
            ['Jun.11', 26759, 15336, 1898, 297],
            ['Sept.11', 27032, 15309, 1893, 298],
            ['Ene.12', 26903, 15786, 1881, 298],
            ['Mar.12', 27306, 15686, 1892, 300],
            ['Jul.12', 27305, 16430, 1903, 304],
            ['Oct.12', 27739, 16846, 2038, 306],
            ['Dic.12', 27778, 16930, 2041, 306],
            ['Feb.13', 28625, 16551, 2113, 306],
            ['May.13', 29523, 16938, 2117, 308],
            ['Jul.13', 29633, 17058, 2133, 308],
            ['Sept.13', 29714, 17073, 2133, 308],
            ['Dic.13', 29771, 16967, 2127, 306],
            ['Mar.14', 29975, 18002, 2213, 347],
            ['Sept.14', 29809, 19707, 2262, 393],
            ['Ene.15', 30240, 20324, 2292, 426],
            ['Abr.15', 30707, 21179, 2328, 457],
            ['Jul.15', 31336, 21589, 2430, 470],
            ['Oct.15', 32131, 21756, 2432, 497],
            ['Ene.16', 32388, 21933, 2482, 537],
            ['Abr.16', 33255, 22616, 2518, 537],
            ['Jul.16', 33310, 22883, 2534, 654],
            ['Oct.16', 33382, 23072, 2552, 664],
            ['Dic.16', 34474, 23345, 2567, 670],
            ['Abr.17', 34472, 24061, 2567, 676],
            ['Jul.17', 34539, 24545, 2585, 676],
            ['Oct.17', 34830, 24683, 2636, 680]
        ]);

        var options_chart = {
            legend: 'none',
            hAxis: {
                title: 'Fecha',
                titleTextStyle: {color: '#6f6f6f'},
                gridlines: {color: '#fff', count: 8},
                textStyle: {color: '#6f6f6f', fontSize: 12}
            },
            vAxis: {
                title: 'Número especies observadas (en miles)',
                titleTextStyle: {color: '#6f6f6f'},
                minValue: 0,
                gridlines: {color: '#fff'},
                format: 'short',
                textStyle: {color: '#6f6f6f', fontSize: 12}
            },
            isStacked: true,
            areaOpacity: 0.7,
            chartArea: {
                backgroundColor: '#E7E9ED',
                left: 70,
                top: 20,
                right: 50,
                width: '100%',
                height: 400
            },
            colors: ['#9A9ACC', '#4BC0C0', '#FF6383', '#FFCE56'],
            width: '100%'
        };

        var chart = new google.visualization.AreaChart(
          document.getElementById('chart_div_records')
        );
        chart.draw(data, options_chart);
    }

    // tabs de graficas especies

    $('.databox input').on('click', function () {
        $('.databox .acttabbtn').removeClass('acttabbtn');
        $(this).addClass('acttabbtn');
    });

    //mapa paises

    google.charts.load('current', {packages: ['geochart']});
    google.charts.setOnLoadCallback(drawRegionsMap);

    function drawRegionsMap() {
        var data = google.visualization.arrayToDataTable([
            ['Country', 'Puesto'],
            ['Brazil', 1],
            ['Indonesia', 2],
            ['Colombia', 3],
            ['China', 4],
            ['Peru', 5],
            ['Mexico', 6],
            ['Australia', 7],
            ['Ecuador', 8],
            ['India', 9],
            ['USA', 10]
        ]);

        var options = {
            displayMode: 'regions',
            enableRegionInteractivity: true,
            backgroundColor: 'transparent',
            colors: ['#F7B000', '#F7F0DE'],
            datalessRegionColor: '#17557A',
            width: '90%',
            legend: 'none',
            tooltip: {
                isHtml: true,
                textStyle: {color: '#fff', fontName: 'Open Sans'}
            }
        };

        var chart = new google.visualization.GeoChart(
          document.getElementById('regions_div')
        );

        chart.draw(data, options);
    }

    //Mapa departamentos especies

    google.charts.load('current', {packages: ['geochart']});
    google.charts.setOnLoadCallback(drawMarkersMap);

    function drawMarkersMap() {
        var data = google.visualization.arrayToDataTable([
            ['Departament', 'Nombre', 'Especies'],
            ['CO-DC', 'Bogotá Distrito Capital', 5612],
            ['CO-AMA', 'Amazonas', 9679],
            ['CO-ANT', 'Antioquia', 20309],
            ['CO-ARA', 'Arauca', 4207],
            ['CO-ATL', 'Atlántico', 2698],
            ['CO-BOL', 'Bolívar', 5768],
            ['CO-BOY', 'Boyacá', 10641],
            ['CO-CAL', 'Caldas', 8740],
            ['CO-CAQ', 'Caquetá', 10299],
            ['CO-CAS', 'Casanare', 5656],
            ['CO-CAU', 'Cauca', 11535],
            ['CO-CES', 'Cesar', 3571],
            ['CO-COR', 'Córdoba', 3886],
            ['CO-CUN', 'Cundinamarca', 15480],
            ['CO-CHO', 'Chocó', 11999],
            ['CO-GUA', 'Guainía', 3780],
            ['CO-GUV', 'Guaviare', 4305],
            ['CO-HUI', 'Huila', 6727],
            ['CO-LAG', 'La Guajira', 4461],
            ['CO-MAG', 'Magdalena', 11216],
            ['CO-MET', 'Meta', 18254],
            ['CO-NAR', 'Nariño', 10080],
            ['CO-NSA', 'Norte de Santander', 6675],
            ['CO-PUT', 'Putumayo', 8030],
            ['CO-QUI', 'Quindío', 4869],
            ['CO-RIS', 'Risaralda', 7401],
            ['CO-ARC', 'San Andrés, Providencia y Santa Catalina', 2116],
            ['CO-SAN', 'Santander', 12575],
            ['CO-SUC', 'Sucre', 3134],
            ['CO-TOL', 'Tolima', 7944],
            ['CO-VAC', 'Valle del Cauca', 16557],
            ['CO-VAU', 'Vaupés', 6158],
            ['CO-VID', 'Vichada', 4499]
        ]);

        var options = {
            region: 'CO',
            resolution: 'provinces',
            displayMode: 'regions',
            enableRegionInteractivity: true,
            backgroundColor: 'transparent',
            colors: ['#3999C2', '#17557A'],
            datalessRegionColor: 'transparent',
            width: '100%',
            tooltip: {
                isHtml: true,
                textStyle: {color: '#fff', fontName: 'Open Sans'}
            }
        };

        var chart = new google.visualization.GeoChart(
          document.getElementById('chart_div')
        );
        chart.draw(data, options);
    }

    //tabs de mapa departamentos

    $('.mapsdep input').on('click', function () {
        $('.mapsdep .acttabbtn').removeClass('acttabbtn');
        $(this).addClass('acttabbtn');
    });
});
